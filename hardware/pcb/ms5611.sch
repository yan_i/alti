EESchema Schematic File Version 2
LIBS:aero-rescue
LIBS:aero
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:aero-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 8
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L ms5611 U4
U 1 1 551EEE6D
P 5350 3550
F 0 "U4" H 5550 3350 60  0000 C CNN
F 1 "ms5611" H 5300 4000 60  0000 C CNN
F 2 "aero:ms5611" H 5350 3550 60  0001 C CNN
F 3 "" H 5350 3550 60  0000 C CNN
	1    5350 3550
	1    0    0    1   
$EndComp
$Comp
L C C19
U 1 1 551EEEE9
P 4500 3700
F 0 "C19" H 4525 3800 50  0000 L CNN
F 1 "100nF" H 4525 3600 50  0000 L CNN
F 2 "Capacitors_SMD:C_0402" H 4538 3550 30  0001 C CNN
F 3 "" H 4500 3700 60  0000 C CNN
	1    4500 3700
	-1   0    0    1   
$EndComp
$Comp
L GND #PWR030
U 1 1 551EEF1B
P 4350 3150
F 0 "#PWR030" H 4350 2900 50  0001 C CNN
F 1 "GND" H 4350 3000 50  0000 C CNN
F 2 "" H 4350 3150 60  0000 C CNN
F 3 "" H 4350 3150 60  0000 C CNN
	1    4350 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3100 4500 3550
Wire Wire Line
	4500 3850 4500 4200
Text HLabel 4900 2950 1    60   Input ~ 0
EN
Wire Wire Line
	4900 3550 4900 2950
Wire Wire Line
	4500 3100 4350 3100
Wire Wire Line
	4350 3100 4350 3150
Wire Wire Line
	4500 4200 4350 4200
Wire Wire Line
	4350 4200 4350 4100
Wire Wire Line
	4900 3650 4700 3650
Wire Wire Line
	4700 3400 4700 3750
Wire Wire Line
	4700 3400 4500 3400
Connection ~ 4500 3400
Wire Wire Line
	4900 3850 4900 3950
Wire Wire Line
	4900 3950 4500 3950
Connection ~ 4500 3950
Wire Wire Line
	4700 3750 4900 3750
Connection ~ 4700 3650
Text HLabel 6300 3750 2    60   Input ~ 0
MOSI
Text HLabel 6300 3650 2    60   Input ~ 0
MISO
Text HLabel 6300 3850 2    60   Input ~ 0
SCLK
Wire Wire Line
	5850 3850 6300 3850
Wire Wire Line
	5850 3750 6300 3750
Wire Wire Line
	6300 3650 5850 3650
Wire Wire Line
	5850 3550 5850 3250
Wire Wire Line
	5850 3250 4900 3250
Connection ~ 4900 3250
$Comp
L +3V #PWR031
U 1 1 55281F01
P 4350 4100
F 0 "#PWR031" H 4350 4000 60  0001 C CNN
F 1 "+3V" H 4200 4250 60  0000 C CNN
F 2 "" H 4350 4100 60  0000 C CNN
F 3 "" H 4350 4100 60  0000 C CNN
	1    4350 4100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
