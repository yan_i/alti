
#ifdef __cplusplus
extern "C" {
#endif

#include <rtos.h>
#include <stdint.h>

typedef uint32_t BaseType_t;

void __empty(void)
{
  return;
}

#ifdef __cplusplus
}
#endif
