# Generated from ./PinDefinition.g4 by ANTLR 4.5.1
# encoding: utf-8
from antlr4 import *
from io import StringIO


from pin_def import *
#el = []

def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\17")
        buf.write("\177\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4")
        buf.write("\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\3\2\6\2\36\n\2\r\2\16\2\37\3\2\3\2\3\2\3\3\3\3\3")
        buf.write("\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\61\n\3\3\4")
        buf.write("\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3")
        buf.write("\6\3\6\3\6\3\6\3\6\3\6\5\6G\n\6\3\7\3\7\3\7\3\7\3\b\3")
        buf.write("\b\3\b\3\b\3\b\3\t\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n")
        buf.write("\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\13\5\13e\n\13")
        buf.write("\3\f\3\f\3\f\3\f\3\f\3\f\5\fm\n\f\3\r\3\r\3\r\3\r\3\r")
        buf.write("\3\r\3\r\3\r\5\rw\n\r\3\16\3\16\3\16\3\16\3\16\3\16\3")
        buf.write("\16\2\2\17\2\4\6\b\n\f\16\20\22\24\26\30\32\2\2y\2\35")
        buf.write("\3\2\2\2\4\60\3\2\2\2\6\62\3\2\2\2\b8\3\2\2\2\nF\3\2\2")
        buf.write("\2\fH\3\2\2\2\16L\3\2\2\2\20Q\3\2\2\2\22W\3\2\2\2\24d")
        buf.write("\3\2\2\2\26l\3\2\2\2\30v\3\2\2\2\32x\3\2\2\2\34\36\5\4")
        buf.write("\3\2\35\34\3\2\2\2\36\37\3\2\2\2\37\35\3\2\2\2\37 \3\2")
        buf.write("\2\2 !\3\2\2\2!\"\7\2\2\3\"#\b\2\1\2#\3\3\2\2\2$%\5\f")
        buf.write("\7\2%&\b\3\1\2&\61\3\2\2\2\'(\5\32\16\2()\b\3\1\2)\61")
        buf.write("\3\2\2\2*+\5\20\t\2+,\b\3\1\2,\61\3\2\2\2-.\5\6\4\2./")
        buf.write("\b\3\1\2/\61\3\2\2\2\60$\3\2\2\2\60\'\3\2\2\2\60*\3\2")
        buf.write("\2\2\60-\3\2\2\2\61\5\3\2\2\2\62\63\7\n\2\2\63\64\7\3")
        buf.write("\2\2\64\65\5\30\r\2\65\66\7\4\2\2\66\67\b\4\1\2\67\7\3")
        buf.write("\2\2\289\7\7\2\29:\7\3\2\2:;\5\n\6\2;<\7\4\2\2<=\b\5\1")
        buf.write("\2=\t\3\2\2\2>?\5\16\b\2?@\b\6\1\2@G\3\2\2\2AB\5\16\b")
        buf.write("\2BC\7\5\2\2CD\5\n\6\2DE\b\6\1\2EG\3\2\2\2F>\3\2\2\2F")
        buf.write("A\3\2\2\2G\13\3\2\2\2HI\7\b\2\2IJ\5\16\b\2JK\b\7\1\2K")
        buf.write("\r\3\2\2\2LM\7\3\2\2MN\5\24\13\2NO\7\4\2\2OP\b\b\1\2P")
        buf.write("\17\3\2\2\2QR\7\t\2\2RS\7\3\2\2ST\5\24\13\2TU\7\4\2\2")
        buf.write("UV\b\t\1\2V\21\3\2\2\2WX\7\f\2\2XY\7\6\2\2YZ\7\f\2\2Z")
        buf.write("[\b\n\1\2[\23\3\2\2\2\\]\5\22\n\2]^\b\13\1\2^e\3\2\2\2")
        buf.write("_`\5\22\n\2`a\7\5\2\2ab\5\24\13\2bc\b\13\1\2ce\3\2\2\2")
        buf.write("d\\\3\2\2\2d_\3\2\2\2e\25\3\2\2\2fg\5\22\n\2gh\b\f\1\2")
        buf.write("hm\3\2\2\2ij\5\b\5\2jk\b\f\1\2km\3\2\2\2lf\3\2\2\2li\3")
        buf.write("\2\2\2m\27\3\2\2\2no\5\26\f\2op\b\r\1\2pw\3\2\2\2qr\5")
        buf.write("\26\f\2rs\7\5\2\2st\5\30\r\2tu\b\r\1\2uw\3\2\2\2vn\3\2")
        buf.write("\2\2vq\3\2\2\2w\31\3\2\2\2xy\7\13\2\2yz\7\3\2\2z{\5\30")
        buf.write("\r\2{|\7\4\2\2|}\b\16\1\2}\33\3\2\2\2\b\37\60Fdlv")
        return buf.getvalue()


class PinDefinitionParser ( Parser ):

    grammarFileName = "PinDefinition.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ "<INVALID>", "'{'", "'}'", "','", "':'", "'pins'", 
                     "'pin'", "'timer'", "'bus'", "'peripheral'" ]

    symbolicNames = [ "<INVALID>", "<INVALID>", "<INVALID>", "<INVALID>", 
                      "<INVALID>", "PINS", "PIN", "TIMER", "BUS", "PERIPHERAL", 
                      "ID", "KEY", "WS", "LINE_COMMENT" ]

    RULE_specification = 0
    RULE_definition = 1
    RULE_bus_def = 2
    RULE_pins_def = 3
    RULE_pin_list = 4
    RULE_single_pin_def = 5
    RULE_pin_def = 6
    RULE_timer_def = 7
    RULE_def_tuple = 8
    RULE_def_tuple_list = 9
    RULE_complex_member = 10
    RULE_complex_member_list = 11
    RULE_peripheral_def = 12

    ruleNames =  [ "specification", "definition", "bus_def", "pins_def", 
                   "pin_list", "single_pin_def", "pin_def", "timer_def", 
                   "def_tuple", "def_tuple_list", "complex_member", "complex_member_list", 
                   "peripheral_def" ]

    EOF = Token.EOF
    T__0=1
    T__1=2
    T__2=3
    T__3=4
    PINS=5
    PIN=6
    TIMER=7
    BUS=8
    PERIPHERAL=9
    ID=10
    KEY=11
    WS=12
    LINE_COMMENT=13

    def __init__(self, input:TokenStream):
        super().__init__(input)
        self.checkVersion("4.5.1")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class SpecificationContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.spec = None
            self._definition = None # DefinitionContext
            self.el = list() # of DefinitionContexts

        def EOF(self):
            return self.getToken(PinDefinitionParser.EOF, 0)

        def definition(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(PinDefinitionParser.DefinitionContext)
            else:
                return self.getTypedRuleContext(PinDefinitionParser.DefinitionContext,i)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_specification

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSpecification" ):
                listener.enterSpecification(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSpecification" ):
                listener.exitSpecification(self)




    def specification(self):

        localctx = PinDefinitionParser.SpecificationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_specification)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 27 
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while True:
                self.state = 26
                localctx._definition = self.definition()
                localctx.el.append(localctx._definition)
                self.state = 29 
                self._errHandler.sync(self)
                _la = self._input.LA(1)
                if not ((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << PinDefinitionParser.PIN) | (1 << PinDefinitionParser.TIMER) | (1 << PinDefinitionParser.BUS) | (1 << PinDefinitionParser.PERIPHERAL))) != 0)):
                    break

            self.state = 31
            self.match(PinDefinitionParser.EOF)
            localctx.spec = [x.e for x in localctx.el]
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DefinitionContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None
            self.a = None # Single_pin_defContext

        def single_pin_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Single_pin_defContext,0)


        def peripheral_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Peripheral_defContext,0)


        def timer_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Timer_defContext,0)


        def bus_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Bus_defContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_definition

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDefinition" ):
                listener.enterDefinition(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDefinition" ):
                listener.exitDefinition(self)




    def definition(self):

        localctx = PinDefinitionParser.DefinitionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_definition)
        try:
            self.state = 46
            token = self._input.LA(1)
            if token in [PinDefinitionParser.PIN]:
                self.enterOuterAlt(localctx, 1)
                self.state = 34
                localctx.a = self.single_pin_def()
                localctx.e = localctx.a.pin 

            elif token in [PinDefinitionParser.PERIPHERAL]:
                self.enterOuterAlt(localctx, 2)
                self.state = 37
                localctx.a = self.peripheral_def()
                localctx.e = localctx.a.e 

            elif token in [PinDefinitionParser.TIMER]:
                self.enterOuterAlt(localctx, 3)
                self.state = 40
                localctx.a = self.timer_def()
                localctx.e = localctx.a.timer

            elif token in [PinDefinitionParser.BUS]:
                self.enterOuterAlt(localctx, 4)
                self.state = 43
                localctx.a = self.bus_def()
                localctx.e = localctx.a.bus

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Bus_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.bus = None
            self.l = None # Complex_member_listContext

        def BUS(self):
            return self.getToken(PinDefinitionParser.BUS, 0)

        def complex_member_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Complex_member_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_bus_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterBus_def" ):
                listener.enterBus_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitBus_def" ):
                listener.exitBus_def(self)




    def bus_def(self):

        localctx = PinDefinitionParser.Bus_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_bus_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 48
            self.match(PinDefinitionParser.BUS)
            self.state = 49
            self.match(PinDefinitionParser.T__0)
            self.state = 50
            localctx.l = self.complex_member_list()
            self.state = 51
            self.match(PinDefinitionParser.T__1)
            localctx.bus = Bus(**localctx.l.e)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Pins_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.pins = None
            self.l = None # Pin_listContext

        def PINS(self):
            return self.getToken(PinDefinitionParser.PINS, 0)

        def pin_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Pin_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_pins_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPins_def" ):
                listener.enterPins_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPins_def" ):
                listener.exitPins_def(self)




    def pins_def(self):

        localctx = PinDefinitionParser.Pins_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_pins_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 54
            self.match(PinDefinitionParser.PINS)
            self.state = 55
            self.match(PinDefinitionParser.T__0)
            self.state = 56
            localctx.l = self.pin_list()
            self.state = 57
            self.match(PinDefinitionParser.T__1)
            localctx.pins = {'pins':PinSet(localctx.l.pins)}
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Pin_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.pins = None
            self.h = None # Pin_defContext
            self.t = None # Pin_listContext

        def pin_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Pin_defContext,0)


        def pin_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Pin_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_pin_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPin_list" ):
                listener.enterPin_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPin_list" ):
                listener.exitPin_list(self)




    def pin_list(self):

        localctx = PinDefinitionParser.Pin_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_pin_list)
        try:
            self.state = 68
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 60
                localctx.h = self.pin_def()
                localctx.pins = [localctx.h.pin]
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 63
                localctx.h = self.pin_def()
                self.state = 64
                self.match(PinDefinitionParser.T__2)
                self.state = 65
                localctx.t = self.pin_list()
                localctx.pins = localctx.t.pins + [localctx.h.pin]
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Single_pin_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.pin = None
            self.p = None # Pin_defContext

        def PIN(self):
            return self.getToken(PinDefinitionParser.PIN, 0)

        def pin_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Pin_defContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_single_pin_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterSingle_pin_def" ):
                listener.enterSingle_pin_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitSingle_pin_def" ):
                listener.exitSingle_pin_def(self)




    def single_pin_def(self):

        localctx = PinDefinitionParser.Single_pin_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_single_pin_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 70
            self.match(PinDefinitionParser.PIN)
            self.state = 71
            localctx.p = self.pin_def()
            localctx.pin = localctx.p.pin
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Pin_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.pin = None
            self.l = None # Def_tuple_listContext

        def def_tuple_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Def_tuple_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_pin_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPin_def" ):
                listener.enterPin_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPin_def" ):
                listener.exitPin_def(self)




    def pin_def(self):

        localctx = PinDefinitionParser.Pin_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_pin_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.match(PinDefinitionParser.T__0)
            self.state = 75
            localctx.l = self.def_tuple_list()
            self.state = 76
            self.match(PinDefinitionParser.T__1)
            localctx.pin = Pin(**localctx.l.e) 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Timer_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.timer = None
            self.l = None # Def_tuple_listContext

        def TIMER(self):
            return self.getToken(PinDefinitionParser.TIMER, 0)

        def def_tuple_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Def_tuple_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_timer_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterTimer_def" ):
                listener.enterTimer_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitTimer_def" ):
                listener.exitTimer_def(self)




    def timer_def(self):

        localctx = PinDefinitionParser.Timer_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_timer_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 79
            self.match(PinDefinitionParser.TIMER)
            self.state = 80
            self.match(PinDefinitionParser.T__0)
            self.state = 81
            localctx.l = self.def_tuple_list()
            self.state = 82
            self.match(PinDefinitionParser.T__1)
            localctx.timer = Timer(**localctx.l.e) 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Def_tupleContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None
            self.f = None # Token
            self.s = None # Token

        def ID(self, i:int=None):
            if i is None:
                return self.getTokens(PinDefinitionParser.ID)
            else:
                return self.getToken(PinDefinitionParser.ID, i)

        def getRuleIndex(self):
            return PinDefinitionParser.RULE_def_tuple

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDef_tuple" ):
                listener.enterDef_tuple(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDef_tuple" ):
                listener.exitDef_tuple(self)




    def def_tuple(self):

        localctx = PinDefinitionParser.Def_tupleContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_def_tuple)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            localctx.f = self.match(PinDefinitionParser.ID)
            self.state = 86
            self.match(PinDefinitionParser.T__3)
            self.state = 87
            localctx.s = self.match(PinDefinitionParser.ID)
            localctx.e = {(None if localctx.f is None else localctx.f.text):(None if localctx.s is None else localctx.s.text)} 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Def_tuple_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None
            self.head = None # Def_tupleContext
            self.tail = None # Def_tuple_listContext

        def def_tuple(self):
            return self.getTypedRuleContext(PinDefinitionParser.Def_tupleContext,0)


        def def_tuple_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Def_tuple_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_def_tuple_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDef_tuple_list" ):
                listener.enterDef_tuple_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDef_tuple_list" ):
                listener.exitDef_tuple_list(self)




    def def_tuple_list(self):

        localctx = PinDefinitionParser.Def_tuple_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_def_tuple_list)
        try:
            self.state = 98
            la_ = self._interp.adaptivePredict(self._input,3,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 90
                localctx.head = self.def_tuple()
                localctx.e = localctx.head.e 
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 93
                localctx.head = self.def_tuple()
                self.state = 94
                self.match(PinDefinitionParser.T__2)
                self.state = 95
                localctx.tail = self.def_tuple_list()
                localctx.head.e.update(localctx.tail.e); localctx.e = localctx.head.e
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Complex_memberContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None
            self.v = None # Def_tupleContext

        def def_tuple(self):
            return self.getTypedRuleContext(PinDefinitionParser.Def_tupleContext,0)


        def pins_def(self):
            return self.getTypedRuleContext(PinDefinitionParser.Pins_defContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_complex_member

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComplex_member" ):
                listener.enterComplex_member(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComplex_member" ):
                listener.exitComplex_member(self)




    def complex_member(self):

        localctx = PinDefinitionParser.Complex_memberContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_complex_member)
        try:
            self.state = 106
            token = self._input.LA(1)
            if token in [PinDefinitionParser.ID]:
                self.enterOuterAlt(localctx, 1)
                self.state = 100
                localctx.v = self.def_tuple()
                localctx.e = localctx.v.e

            elif token in [PinDefinitionParser.PINS]:
                self.enterOuterAlt(localctx, 2)
                self.state = 103
                localctx.v = self.pins_def()
                localctx.e = localctx.v.pins

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Complex_member_listContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None
            self.head = None # Complex_memberContext
            self.tail = None # Complex_member_listContext

        def complex_member(self):
            return self.getTypedRuleContext(PinDefinitionParser.Complex_memberContext,0)


        def complex_member_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Complex_member_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_complex_member_list

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterComplex_member_list" ):
                listener.enterComplex_member_list(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitComplex_member_list" ):
                listener.exitComplex_member_list(self)




    def complex_member_list(self):

        localctx = PinDefinitionParser.Complex_member_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_complex_member_list)
        try:
            self.state = 116
            la_ = self._interp.adaptivePredict(self._input,5,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 108
                localctx.head = self.complex_member()
                localctx.e = localctx.head.e
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 111
                localctx.head = self.complex_member()
                self.state = 112
                self.match(PinDefinitionParser.T__2)
                self.state = 113
                localctx.tail = self.complex_member_list()
                localctx.head.e.update(localctx.tail.e); localctx.e = localctx.head.e
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Peripheral_defContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser
            self.e = None
            self.l = None # Complex_member_listContext

        def PERIPHERAL(self):
            return self.getToken(PinDefinitionParser.PERIPHERAL, 0)

        def complex_member_list(self):
            return self.getTypedRuleContext(PinDefinitionParser.Complex_member_listContext,0)


        def getRuleIndex(self):
            return PinDefinitionParser.RULE_peripheral_def

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterPeripheral_def" ):
                listener.enterPeripheral_def(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitPeripheral_def" ):
                listener.exitPeripheral_def(self)




    def peripheral_def(self):

        localctx = PinDefinitionParser.Peripheral_defContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_peripheral_def)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 118
            self.match(PinDefinitionParser.PERIPHERAL)
            self.state = 119
            self.match(PinDefinitionParser.T__0)
            self.state = 120
            localctx.l = self.complex_member_list()
            self.state = 121
            self.match(PinDefinitionParser.T__1)
            localctx.e = Peripheral(**localctx.l.e) 
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





