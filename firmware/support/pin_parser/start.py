#!/usr/bin/env python3

import sys
import textwrap

from antlr4 import *
from pin_def import *
from PinDefinitionLexer import PinDefinitionLexer
from PinDefinitionParser import PinDefinitionParser


class OpenCM3Render(object):

    HEADER_NAME = '__PINS_DEF'
    F = '#define {0:30}  {1}\n'


    def __init__(self, f, source):
        self._f = f
        self._source = source
        self._render_header()

    def _render_header(self):
        self._f.write('#ifndef {}\n'.format(self.HEADER_NAME))
        self._f.write('#define {}\n'.format(self.HEADER_NAME))
        
        header = '/** AUTOGENERATED from {0}. DO NOT EDIT *********/\n'.format(
                self._source)
        asterisks = len(header) - 3
        border = '/' + ('*' * asterisks) + '/\n'
        header = border + header + border
        self._f.write(header)

        includes = textwrap.dedent('''
            #if defined(STM32L1) && defined(OPENCM3)
            #  include <stm32l1_opencm3_pins.h>
            #elif defined(STM32L1) &&  defined(STM32_STDPERIPH_LIB)
            #  include <stm32l1_stdperiphlib_pins.h>
            #elif defined(TESTING)
            #  include <empty_pins.h>
            #else
            #  error "Unsupprted architecture"
            #endif
        ''')
        self._f.write(includes)

    def render_pin(self, pin):
        self._f.write('#define {0.name:30}  PIN({0.index})\n'.format(pin))
        self._f.write(self.F.format(pin.name+'_GPIO', 'GPIO({})'.format(pin.gpio)))

    def render_peripheral(self, p):
        'XXX Does not render pins'
        self._f.write(self.F.format(p.name+'_LSB', [0, 1][p.bitorder == 'lsb']))
        self._f.write(self.F.format(p.name+'_BUS', p.on_bus))

    def render_bus(self, bus):
        self._f.write(self.F.format(bus.name, bus.kind))
        self._f.write(self.F.format(bus.name+'_GPIO', 'GPIO({})'.format(bus.gpio)))
        #self._f.write(self.F.format(bus.name+'_RCC', 'RCC_' + bus.kind))

        pins = ['PIN({})'.format(p.index) for p in bus.pins]
        self._f.write(self.F.format(bus.name+'_PINS', '('+'|'.join(pins)+')'))

    def render_timer(self, timer):
        n = timer.name
        self._f.write('#define {0:30}  TIM{1}\n'.format(n+'_TIMER', timer.index))
        self._f.write('#define {0:30}  TIM_OC({1})\n'.format(n+'_CHANNEL', timer.channel))
        self._f.write('#define {0:30}  GPIO_AF{1}\n'.format(n+'_AF', timer.af))

    def finish(self):
        self._f.write('#endif /* {} */\n'.format(self.HEADER_NAME))

class MyGrammarListener(ParseTreeListener):
    def __init__(self, renderer):
        super().__init__()
        self._renderer = renderer
        self._banks = GpioBank()

    def on_pin(self, pin):
        self._banks.add_pin(pin)

    def on_bus(self, bus):
        bus.pins.set_name(bus.name)
        bus.pins.set_gpio(bus.gpio)
        self._banks.add_set(bus.pins)
        self._renderer.render_bus(bus)

    def on_peripheral(self, peripheral):
        peripheral.pins.set_name(peripheral.name)
        self._banks.add_set(peripheral.pins)
        self._renderer.render_peripheral(peripheral)

    def on_timer(self, timer):
        self._renderer.render_timer(timer)

    def enterSpecification(self, ctx):
        for v in ctx.spec:
            if isinstance(v, Pin): self.on_pin(v)
            elif isinstance(v, Bus): self.on_bus(v)
            elif isinstance(v, Peripheral): self.on_peripheral(v)
            elif isinstance(v, Timer): self.on_timer(v)
        self._banks.visit_pins(self._renderer.render_pin)
 

source = '../include/pins.def'
input = FileStream(source)
lexer = PinDefinitionLexer(input)
stream = CommonTokenStream(lexer)
parser = PinDefinitionParser(stream)
tree = parser.specification()

renderer = OpenCM3Render(sys.stdout, source)
listener = MyGrammarListener(renderer)
walker = ParseTreeWalker()
walker.walk(listener, tree)
renderer.finish()

