# Generated from ./PinDefinition.g4 by ANTLR 4.5.1
from antlr4 import *
from io import StringIO


from pin_def import *
#el = []


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\17")
        buf.write("z\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7")
        buf.write("\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16")
        buf.write("\t\16\4\17\t\17\4\20\t\20\3\2\3\2\3\3\3\3\3\4\3\4\3\5")
        buf.write("\3\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3")
        buf.write("\b\3\b\3\b\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\n")
        buf.write("\3\n\3\n\3\n\3\n\3\13\3\13\3\f\3\f\3\r\3\r\6\rN\n\r\r")
        buf.write("\r\16\rO\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16\3\16")
        buf.write("\3\16\3\16\3\16\5\16i\n\16\3\17\6\17l\n\17\r\17\16\17")
        buf.write("m\3\17\3\17\3\20\3\20\7\20t\n\20\f\20\16\20w\13\20\3\20")
        buf.write("\3\20\2\2\21\3\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\23\13")
        buf.write("\25\2\27\2\31\f\33\r\35\16\37\17\3\2\5\5\2C\\aac|\5\2")
        buf.write("\13\f\16\17\"\"\4\2\f\f\17\17\177\2\3\3\2\2\2\2\5\3\2")
        buf.write("\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2")
        buf.write("\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\31\3\2\2\2\2")
        buf.write("\33\3\2\2\2\2\35\3\2\2\2\2\37\3\2\2\2\3!\3\2\2\2\5#\3")
        buf.write("\2\2\2\7%\3\2\2\2\t\'\3\2\2\2\13)\3\2\2\2\r.\3\2\2\2\17")
        buf.write("\62\3\2\2\2\218\3\2\2\2\23<\3\2\2\2\25G\3\2\2\2\27I\3")
        buf.write("\2\2\2\31M\3\2\2\2\33h\3\2\2\2\35k\3\2\2\2\37q\3\2\2\2")
        buf.write("!\"\7}\2\2\"\4\3\2\2\2#$\7\177\2\2$\6\3\2\2\2%&\7.\2\2")
        buf.write("&\b\3\2\2\2\'(\7<\2\2(\n\3\2\2\2)*\7r\2\2*+\7k\2\2+,\7")
        buf.write("p\2\2,-\7u\2\2-\f\3\2\2\2./\7r\2\2/\60\7k\2\2\60\61\7")
        buf.write("p\2\2\61\16\3\2\2\2\62\63\7v\2\2\63\64\7k\2\2\64\65\7")
        buf.write("o\2\2\65\66\7g\2\2\66\67\7t\2\2\67\20\3\2\2\289\7d\2\2")
        buf.write("9:\7w\2\2:;\7u\2\2;\22\3\2\2\2<=\7r\2\2=>\7g\2\2>?\7t")
        buf.write("\2\2?@\7k\2\2@A\7r\2\2AB\7j\2\2BC\7g\2\2CD\7t\2\2DE\7")
        buf.write("c\2\2EF\7n\2\2F\24\3\2\2\2GH\t\2\2\2H\26\3\2\2\2IJ\4\62")
        buf.write(";\2J\30\3\2\2\2KN\5\25\13\2LN\5\27\f\2MK\3\2\2\2ML\3\2")
        buf.write("\2\2NO\3\2\2\2OM\3\2\2\2OP\3\2\2\2P\32\3\2\2\2QR\7p\2")
        buf.write("\2RS\7c\2\2ST\7o\2\2Ti\7g\2\2UV\7d\2\2VW\7w\2\2Wi\7u\2")
        buf.write("\2XY\7i\2\2YZ\7r\2\2Z[\7k\2\2[i\7q\2\2\\]\7k\2\2]^\7p")
        buf.write("\2\2^_\7f\2\2_`\7g\2\2`i\7z\2\2ab\7e\2\2bc\7j\2\2cd\7")
        buf.write("c\2\2de\7p\2\2ef\7p\2\2fg\7g\2\2gi\7n\2\2hQ\3\2\2\2hU")
        buf.write("\3\2\2\2hX\3\2\2\2h\\\3\2\2\2ha\3\2\2\2i\34\3\2\2\2jl")
        buf.write("\t\3\2\2kj\3\2\2\2lm\3\2\2\2mk\3\2\2\2mn\3\2\2\2no\3\2")
        buf.write("\2\2op\b\17\2\2p\36\3\2\2\2qu\7%\2\2rt\n\4\2\2sr\3\2\2")
        buf.write("\2tw\3\2\2\2us\3\2\2\2uv\3\2\2\2vx\3\2\2\2wu\3\2\2\2x")
        buf.write("y\b\20\2\2y \3\2\2\2\b\2MOhmu\3\2\3\2")
        return buf.getvalue()


class PinDefinitionLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]


    T__0 = 1
    T__1 = 2
    T__2 = 3
    T__3 = 4
    PINS = 5
    PIN = 6
    TIMER = 7
    BUS = 8
    PERIPHERAL = 9
    ID = 10
    KEY = 11
    WS = 12
    LINE_COMMENT = 13

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
            "'{'", "'}'", "','", "':'", "'pins'", "'pin'", "'timer'", "'bus'", 
            "'peripheral'" ]

    symbolicNames = [ "<INVALID>",
            "PINS", "PIN", "TIMER", "BUS", "PERIPHERAL", "ID", "KEY", "WS", 
            "LINE_COMMENT" ]

    ruleNames = [ "T__0", "T__1", "T__2", "T__3", "PINS", "PIN", "TIMER", 
                  "BUS", "PERIPHERAL", "LETTER", "DIGIT", "ID", "KEY", "WS", 
                  "LINE_COMMENT" ]

    grammarFileName = "PinDefinition.g4"

    def __init__(self, input=None):
        super().__init__(input)
        self.checkVersion("4.5.1")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


