#ifndef __PERIPH_H
#define __PERIPH_H

void arch_config_io(void);

void enable_piezo(void);

void disable_piezo(void);

void enable_pulse(void);

void disable_pulse(void);
#endif
