/**
* This file is autogenerated by nRFgo Studio 1.18.0.9
*/

#ifndef SETUP_MESSAGES_H__
#define SETUP_MESSAGES_H__

#include "hal_platform.h"
#include "aci.h"


#define SETUP_ID 0
#define SETUP_FORMAT 3 /** nRF8001 D */
#define ACI_DYNAMIC_DATA_SIZE 267

/* Service: Sensor Stream - Characteristic: Sensor Data - Pipe: TX */
#define PIPE_SENSOR_STREAM_SENSOR_DATA_TX          1
#define PIPE_SENSOR_STREAM_SENSOR_DATA_TX_MAX_SIZE 20

/* Service: Sensor Stream - Characteristic: Sensor Data - Pipe: SET */
#define PIPE_SENSOR_STREAM_SENSOR_DATA_SET          2
#define PIPE_SENSOR_STREAM_SENSOR_DATA_SET_MAX_SIZE 20

/* Service: Data - Characteristic: UART TX - Pipe: TX */
#define PIPE_DATA_UART_TX_TX          3
#define PIPE_DATA_UART_TX_TX_MAX_SIZE 20

/* Service: Data - Characteristic: UART RX - Pipe: RX */
#define PIPE_DATA_UART_RX_RX          4
#define PIPE_DATA_UART_RX_RX_MAX_SIZE 20

/* Service: Data - Characteristic: UART Control Point - Pipe: TX */
#define PIPE_DATA_UART_CONTROL_POINT_TX          5
#define PIPE_DATA_UART_CONTROL_POINT_TX_MAX_SIZE 9

/* Service: Data - Characteristic: UART Control Point - Pipe: RX */
#define PIPE_DATA_UART_CONTROL_POINT_RX          6
#define PIPE_DATA_UART_CONTROL_POINT_RX_MAX_SIZE 9

/* Service: Data - Characteristic: UART Link Timing Current - Pipe: SET */
#define PIPE_DATA_UART_LINK_TIMING_CURRENT_SET          7
#define PIPE_DATA_UART_LINK_TIMING_CURRENT_SET_MAX_SIZE 6

/* Service: Aero Config - Characteristic: Aero Config - Pipe: RX_ACK_AUTO */
#define PIPE_AERO_CONFIG_AERO_CONFIG_RX_ACK_AUTO          8
#define PIPE_AERO_CONFIG_AERO_CONFIG_RX_ACK_AUTO_MAX_SIZE 20

/* Service: Aero Config - Characteristic: Aero Config - Pipe: TX */
#define PIPE_AERO_CONFIG_AERO_CONFIG_TX_1          9
#define PIPE_AERO_CONFIG_AERO_CONFIG_TX_1_MAX_SIZE 20


#define NUMBER_OF_PIPES 9

#define SERVICES_PIPE_TYPE_MAPPING_CONTENT {\
  {ACI_STORE_LOCAL, ACI_TX},   \
  {ACI_STORE_LOCAL, ACI_SET},   \
  {ACI_STORE_LOCAL, ACI_TX},   \
  {ACI_STORE_LOCAL, ACI_RX},   \
  {ACI_STORE_LOCAL, ACI_TX},   \
  {ACI_STORE_LOCAL, ACI_RX},   \
  {ACI_STORE_LOCAL, ACI_SET},   \
  {ACI_STORE_LOCAL, ACI_RX_ACK_AUTO},   \
  {ACI_STORE_LOCAL, ACI_TX},   \
}

#define GAP_PPCP_MAX_CONN_INT 0x6 /**< Maximum connection interval as a multiple of 1.25 msec , 0xFFFF means no specific value requested */
#define GAP_PPCP_MIN_CONN_INT  0x6 /**< Minimum connection interval as a multiple of 1.25 msec , 0xFFFF means no specific value requested */
#define GAP_PPCP_SLAVE_LATENCY 0
#define GAP_PPCP_CONN_TIMEOUT 0xffff /** Connection Supervision timeout multiplier as a multiple of 10msec, 0xFFFF means no specific value requested */

#define NB_SETUP_MESSAGES 46
#define SETUP_MESSAGES_CONTENT {\
    {0x00,\
        {\
            0x07,0x06,0x00,0x00,0x03,0x02,0x42,0x07,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x10,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x07,0x00,0x09,0x01,0x01,0x00,0x00,0x06,0x00,0x01,\
            0x11,0x0a,0x18,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x10,0x1c,0x01,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
            0x00,0x00,0x00,0x10,0x00,0x00,0x00,0x1a,0x03,0x90,0x02,0xff,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x10,0x38,0xff,0xff,0x02,0x58,0x0a,0x05,0x00,0x00,0x00,0x10,0x00,0x00,0x00,0x00,0x00,0x00,\
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x05,0x06,0x10,0x54,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0x00,0x04,0x04,0x02,0x02,0x00,0x01,0x28,0x00,0x01,0x00,0x18,0x04,0x04,0x05,0x05,0x00,\
            0x02,0x28,0x03,0x01,0x02,0x03,0x00,0x00,0x2a,0x04,0x04,0x14,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0x1c,0x04,0x00,0x03,0x2a,0x00,0x01,0x41,0x65,0x72,0x6f,0x69,0x63,0x73,0x65,0x6d,0x69,\
            0x2e,0x63,0x6f,0x6d,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x04,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0x38,0x05,0x05,0x00,0x04,0x28,0x03,0x01,0x02,0x05,0x00,0x01,0x2a,0x06,0x04,0x03,0x02,\
            0x00,0x05,0x2a,0x01,0x01,0x00,0x00,0x04,0x04,0x05,0x05,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0x54,0x06,0x28,0x03,0x01,0x02,0x07,0x00,0x04,0x2a,0x06,0x04,0x09,0x08,0x00,0x07,0x2a,\
            0x04,0x01,0x06,0x00,0x06,0x00,0x00,0x00,0xff,0xff,0x04,0x04,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0x70,0x02,0x02,0x00,0x08,0x28,0x00,0x01,0x01,0x18,0x04,0x04,0x10,0x10,0x00,0x09,0x28,\
            0x00,0x01,0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,0xbc,0x53,0x40,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0x8c,0xdc,0x9d,0x01,0x00,0x1b,0x70,0x04,0x04,0x13,0x13,0x00,0x0a,0x28,0x03,0x01,0x12,\
            0x0b,0x00,0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,0xbc,0x53,0x40,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0xa8,0xdc,0x9d,0x02,0x00,0x1b,0x70,0x14,0x04,0x14,0x04,0x00,0x0b,0x00,0x02,0x03,0x00,\
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0xc4,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x46,0x14,0x03,0x02,0x00,0x0c,0x29,0x02,0x01,\
            0x00,0x00,0x04,0x04,0x02,0x02,0x00,0x0d,0x28,0x00,0x01,0x0a,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0xe0,0x18,0x04,0x04,0x05,0x05,0x00,0x0e,0x28,0x03,0x01,0x02,0x0f,0x00,0x24,0x2a,0x06,\
            0x04,0x09,0x08,0x00,0x0f,0x2a,0x24,0x01,0x56,0x65,0x72,0x20,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x20,0xfc,0x30,0x2e,0x30,0x31,0x06,0x04,0x08,0x07,0x00,0x10,0x29,0x04,0x01,0x19,0x00,0x00,\
            0x00,0x01,0x00,0x00,0x04,0x04,0x05,0x05,0x00,0x11,0x28,0x03,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0x18,0x01,0x02,0x12,0x00,0x25,0x2a,0x06,0x04,0x05,0x04,0x00,0x12,0x2a,0x25,0x01,0x30,\
            0x30,0x30,0x30,0x06,0x04,0x08,0x07,0x00,0x13,0x29,0x04,0x01,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0x34,0x19,0x00,0x00,0x00,0x01,0x00,0x00,0x04,0x04,0x05,0x05,0x00,0x14,0x28,0x03,0x01,\
            0x02,0x15,0x00,0x27,0x2a,0x06,0x04,0x03,0x02,0x00,0x15,0x2a,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0x50,0x27,0x01,0x44,0x47,0x06,0x04,0x08,0x07,0x00,0x16,0x29,0x04,0x01,0x19,0x00,0x00,\
            0x00,0x01,0x00,0x00,0x04,0x04,0x05,0x05,0x00,0x17,0x28,0x03,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0x6c,0x01,0x02,0x18,0x00,0x29,0x2a,0x06,0x04,0x15,0x14,0x00,0x18,0x2a,0x29,0x01,0x79,\
            0x61,0x6e,0x40,0x73,0x72,0x74,0x64,0x2e,0x6f,0x72,0x67,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0x88,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x06,0x04,0x08,0x07,0x00,0x19,0x29,0x04,0x01,\
            0x19,0x00,0x00,0x00,0x01,0x00,0x00,0x04,0x04,0x10,0x10,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0xa4,0x1a,0x28,0x00,0x01,0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,0xbc,0x53,0x40,0xdc,0x9d,\
            0x03,0x00,0x1b,0x70,0x04,0x04,0x13,0x13,0x00,0x1b,0x28,0x03,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0xc0,0x01,0x10,0x1c,0x00,0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,\
            0x03,0x00,0x40,0x6e,0x14,0x00,0x14,0x00,0x00,0x1c,0x00,0x03,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0xdc,0x02,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
            0x00,0x00,0x00,0x00,0x00,0x46,0x14,0x03,0x02,0x00,0x1d,0x29,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x21,0xf8,0x02,0x01,0x00,0x00,0x04,0x04,0x13,0x13,0x00,0x1e,0x28,0x03,0x01,0x04,0x1f,0x00,\
            0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0x14,0x02,0x00,0x40,0x6e,0x44,0x10,0x14,0x00,0x00,0x1f,0x00,0x02,0x02,0x00,0x00,0x00,\
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0x30,0x00,0x00,0x00,0x00,0x00,0x04,0x04,0x13,0x13,0x00,0x20,0x28,0x03,0x01,0x14,0x21,\
            0x00,0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0x4c,0xb5,0x04,0x00,0x40,0x6e,0x54,0x10,0x09,0x00,0x00,0x21,0x00,0x04,0x02,0x00,0x00,\
            0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x46,0x14,0x03,0x02,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0x68,0x22,0x29,0x02,0x01,0x00,0x00,0x04,0x04,0x13,0x13,0x00,0x23,0x28,0x03,0x01,0x02,\
            0x24,0x00,0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0x84,0xa3,0xb5,0x05,0x00,0x40,0x6e,0x06,0x04,0x07,0x06,0x00,0x24,0x00,0x05,0x02,0xff,\
            0xff,0xff,0xff,0xff,0xff,0x04,0x04,0x02,0x02,0x00,0x25,0x28,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0xa0,0x00,0x01,0x0f,0x18,0x04,0x04,0x05,0x05,0x00,0x26,0x28,0x03,0x01,0x02,0x27,0x00,\
            0x19,0x2a,0x06,0x04,0x02,0x01,0x00,0x27,0x2a,0x19,0x01,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0xbc,0x04,0x04,0x05,0x05,0x00,0x28,0x28,0x03,0x01,0x02,0x29,0x00,0x1a,0x2a,0x06,0x04,\
            0x02,0x01,0x00,0x29,0x2a,0x1a,0x01,0x00,0x04,0x04,0x02,0x02,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0xd8,0x00,0x2a,0x28,0x00,0x01,0x05,0x18,0x04,0x04,0x05,0x05,0x00,0x2b,0x28,0x03,0x01,\
            0x00,0x2c,0x00,0x2b,0x2a,0x06,0x00,0x0b,0x0a,0x00,0x2c,0x2a,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x22,0xf4,0x2b,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x04,0x05,0x05,\
            0x00,0x2d,0x28,0x03,0x01,0x00,0x2e,0x00,0x14,0x2a,0x06,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x23,0x10,0x05,0x04,0x00,0x2e,0x2a,0x14,0x01,0x00,0x00,0x00,0x00,0x04,0x04,0x10,0x10,0x00,\
            0x2f,0x28,0x00,0x01,0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,0xbc,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x23,0x2c,0x53,0x40,0xdc,0x9d,0x04,0x00,0x1b,0x70,0x04,0x04,0x13,0x13,0x00,0x30,0x28,0x03,\
            0x01,0x08,0x31,0x00,0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,0xbc,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x23,0x48,0x53,0x40,0xdc,0x9d,0x06,0x00,0x1b,0x70,0x44,0x10,0x14,0x00,0x00,0x31,0x00,0x06,\
            0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x23,0x64,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x04,0x04,0x13,0x13,0x00,0x32,0x28,\
            0x03,0x01,0x10,0x33,0x00,0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x23,0x80,0xbc,0x53,0x40,0xdc,0x9d,0x05,0x00,0x1b,0x70,0x14,0x00,0x14,0x00,0x00,0x33,0x00,\
            0x05,0x03,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x19,0x06,0x23,0x9c,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x46,0x14,0x03,0x02,0x00,0x34,\
            0x29,0x02,0x01,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x40,0x00,0x00,0x02,0x03,0x00,0x82,0x04,0x00,0x0b,0x00,0x0c,0x00,0x03,0x02,0x00,0x02,0x04,\
            0x00,0x1c,0x00,0x1d,0x00,0x02,0x02,0x00,0x08,0x04,0x00,0x1f,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x40,0x1c,0x00,0x00,0x00,0x04,0x02,0x00,0x0a,0x04,0x00,0x21,0x00,0x22,0x00,0x05,0x02,0x00,\
            0x80,0x04,0x00,0x24,0x00,0x00,0x00,0x06,0x03,0x04,0x00,0x04,\
        },\
    },\
    {0x00,\
        {\
            0x11,0x06,0x40,0x38,0x00,0x31,0x00,0x00,0x00,0x05,0x03,0x00,0x02,0x04,0x00,0x33,0x00,0x34,\
        },\
    },\
    {0x00,\
        {\
            0x1f,0x06,0x50,0x00,0x9e,0xca,0xdc,0x24,0x0e,0xe5,0xa9,0xe0,0x93,0xf3,0xa3,0xb5,0x00,0x00,0x40,0x6e,\
            0xf7,0x65,0x29,0x97,0x0a,0x41,0x77,0xbc,0x53,0x40,0xdc,0x9d,\
        },\
    },\
    {0x00,\
        {\
            0x07,0x06,0x50,0x1c,0x00,0x00,0x1b,0x70,\
        },\
    },\
    {0x00,\
        {\
            0x18,0x06,0x60,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,\
            0x00,0x00,0x00,0x00,0x00,\
        },\
    },\
    {0x00,\
        {\
            0x06,0x06,0xf0,0x00,0x03,0x0c,0x8e,\
        },\
    },\
}

#endif
